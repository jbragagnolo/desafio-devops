
- Fiz as configurações de imagem de container com Dockerfile e o provisionamento com Terraform.
    - Apesar de nunca ter trabalhado em produção com Terraform, optei por utiliza-lo pela facilidade de efetuar o deploy do container e manutenção futura.
- Em um serviço de cloud (AWS, GCP, etc), iria configurar um Load Balance e Auto Scaling dos containers, e consequentemente configurar um webhook para atualizar os containers após o "push" no projeto.


```
$ terraform init
$ terraform apply

$ curl -sv 127.0.0.1:8000/api/comment/new -X POST -H 'Content-Type: application/json' -d '{"email":"alice@example.com","comment":"first post!","content_id":1}'
*   Trying 127.0.0.1:8000...
* TCP_NODELAY set
* Connected to 127.0.0.1 (127.0.0.1) port 8000 (#0)
> POST /api/comment/new HTTP/1.1
> Host: 127.0.0.1:8000
> User-Agent: curl/7.68.0
> Accept: */*
> Content-Type: application/json
> Content-Length: 68
> 
* upload completely sent off: 68 out of 68 bytes
* Mark bundle as not supporting multiuse
< HTTP/1.1 200 OK
< Server: gunicorn/20.0.4
< Date: Fri, 26 Nov 2021 17:25:49 GMT
< Connection: close
< Content-Type: application/json
< Content-Length: 92
< 
{
  "message": "comment created and associated with content_id 1", 
  "status": "SUCCESS"
}
* Closing connection 0

$ terraform destroy
```