terraform {
  required_providers {
    docker = {
      source  = "kreuzwerker/docker"
      version = "~> 2.13.0"
    }
  }
}

provider "docker" {}

resource "docker_image" "desafio-devops" {
  name         = "desafio-devops:latest"
  keep_locally = false
  build {
    path = "."
    tag  = ["desafio-devops:latest"]    
    label = {
      author : "josue"
    }
  }
}

resource "docker_container" "desafio-devops" {
  image = docker_image.desafio-devops.latest
  name  = "desafio-devops"
  restart = "always"
  ports {
    internal = 8000
    external = 8000
  }
}
